//HEY I'M MAKING CHANGES BYATCHES
//object to hold the current state of the game
var GameState = function() {
    this.isGoing = true;
    //store our stuff in an array.  It's arranged like
    // 0 1 2
    // 3 4 5
    // 6 7 8
    //values are either X, O, or blank if not taken.  Should only ever be 9!
    this.squares = [];
    for(var i = 0; i < 9; i++) {
        this.squares.push('');
    }

    //which combos are a win if they're there with the same values
    var winCombos = [
        //horizontal
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        //vertical
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        //diag
        [0, 4, 8],
        [2, 4, 6]
    ];

    var currentPlayerIsFirstPlayer = true;

    //which ever player is playing, switch them
    this.switchPlayer = function() {
        if(currentPlayerIsFirstPlayer) {
            currentPlayerIsFirstPlayer = false;
        }
        else {
            currentPlayerIsFirstPlayer = true;
        }
    };

    this.getCurrentPlayerSymbol = function () {
        if(currentPlayerIsFirstPlayer){
            return 'X';
        }
        else {
            return 'O';
        }
    }

    //if X's combo wins, return X.  If O's does, return O.  Otherwise return ''
    var doesComboWin = function (squares, winCombo) {
        //get the positions we want to evaluate
        var firstWinPos = winCombo[0];
        var secondWinPos = winCombo[1];
        var thirdWinPos = winCombo[2];

        if(squares[firstWinPos] === '' || squares[secondWinPos] === '' || squares[thirdWinPos] === '') {
            return '';
        }

        if(squares[firstWinPos] === squares[secondWinPos] && squares[secondWinPos] === squares[thirdWinPos]){
            //we have a winner, is it one or two?
            return squares[firstWinPos];
        }

        //didn't match all items, no win
        return '';
    };

    var checkForTie = function(squares) {
        var i;
        var thisSquare;
        for (i = 0; i< squares.length; i++ ){
            thisSquare = squares[i];
            if(thisSquare === '') {
                return '';
            }
        }
        return 'tied';
    };

    //can return X for winner is X, O for winner is 0, true for tied, or false for still going
    this.checkForWin = function (){
        //see if we have a win condition
        var res = '';
        //check each possible win
        for(var i = 0; i < winCombos.length; i++) {
            res = doesComboWin(this.squares, winCombos[i]);
            if(res) {
                //we have a winner, don't bother looking further
                this.isGoing = false;
                return res;
            }
        }

        //see if we have a tie!
        res = checkForTie(this.squares);

        //if we got here, we're tied!
        if(res === 'tied') {
            this.isGoing = false;
        }
        return res;
    }
}

//event handler for when they click a button
var buttonClicked = function(id, gameState) {
    if(gameState.isGoing){
        //see what our current value is
        if(gameState.squares[id] !== '') {
            //not open, ignore it!
            return;
        }
        else
        {
            var myImg = document.getElementById('square' + id);
            var span= document.getElementById('status');
            //assign the square to the current player
            var player =  gameState.getCurrentPlayerSymbol();
            gameState.squares[id] = player;
            //also update the HTML page here!
            myImg.src = player + ".png";
            //check for a win
            var res = gameState.checkForWin();
            switch(res){
                case 'X':
                    //x wins, update page!
                    span.innerHTML = 'X wins!';
                break;
                case 'O':
                    span.innerHTML = 'O wins!';
                break;
                case 'tied':
                    //must be a tie, update page
                    span.innerHTML = "It's a tie!";
                break;
                default :
                    //switch players
                    gameState.switchPlayer();

                    player = gameState.getCurrentPlayerSymbol();
                    span.innerHTML = "It's " + player + "'s turn";
                break;
            };
        }
    }
};

//main entry point
var gameState = new GameState();

var sq0Clicked = function (){
    buttonClicked(0, gameState);
};
var sq1Clicked = function (){
    buttonClicked(1, gameState);
};
var sq2Clicked = function (){
    buttonClicked(2, gameState);
};
var sq3Clicked = function (){
    buttonClicked(3, gameState);
};
var sq4Clicked = function (){
    buttonClicked(4, gameState);
};
var sq5Clicked = function (){
    buttonClicked(5, gameState);
};
var sq6Clicked = function (){
    buttonClicked(6, gameState);
};
var sq7Clicked = function (){
    buttonClicked(7, gameState);
};
var sq8Clicked = function (){
    buttonClicked(8, gameState);
};


